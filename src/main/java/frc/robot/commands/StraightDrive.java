// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.NavX;

public class StraightDrive extends CommandBase {
  /** Creates a new StraightDrive. */
  private final Drivetrain drivetrain;
  private final NavX navX;
  private final double SPEED;
  private final double DISTANCE;
  private double navXStartPosition;

  public StraightDrive(Drivetrain drivetrain, NavX navX, double speed, double distance) {
    this.drivetrain = drivetrain;
    this.navX = navX;
    this.SPEED = speed;
    this.DISTANCE = (distance / (Math.PI * 5.0)) * 4096.0; // 5.0 is wheel diameter in inches
    addRequirements(drivetrain, navX);
  }

  @Override
  public void initialize() {
    DriverStation.reportError("SD Started", false);
    this.navX.reset();
    this.navXStartPosition = navX.getYaw();
    drivetrain.resetEncoders();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // drivetrain.arcadeDrive(SPEED, -navX.getYaw() * 0.05);
    SmartDashboard.putNumber("SD NavX offset", Math.abs(navXStartPosition - navX.getYaw()));
    drivetrain.arcadeDrive(SPEED, 0.05 * (navXStartPosition - navX.getYaw()));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    DriverStation.reportError("SD Done", false);
    drivetrain.resetEncoders();
    drivetrain.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    SmartDashboard.putNumber("StraightDrive distance", drivetrain.getPosition());

    return Math.abs(drivetrain.getPosition()) > DISTANCE;
  }

}
