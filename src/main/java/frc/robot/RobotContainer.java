// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.team6479.lib.commands.TeleopTankDrive;
import com.team6479.lib.controllers.CBJoystick;
import com.team6479.lib.controllers.CBXboxController;
import com.team6479.lib.util.dynamic.Limelight;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.commands.TeleopIntakeArm;
import frc.robot.commands.TeleopTurretControl;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.IntakeArm;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.Turret;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private final IntakeArm intakeArm = new IntakeArm();
  private final IntakeArmSolenoid intakeArmSolenoid = new IntakeArmSolenoid();

  private CBXboxController xbox =  new CBXboxController(0);
  private final CBJoystick joystick = new CBJoystick(1);

  private Drivetrain drivetrain = new Drivetrain();
  private Turret turret = new Turret(0, 360); // change limits later
  private Limelight limelight = new Limelight();

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {

    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    drivetrain.setDefaultCommand(new TeleopTankDrive(drivetrain, () -> xbox.getLeftY(), () -> xbox.getRightX()));

    intakeArm.setDefaultCommand(new TeleopIntakeArm(intakeArm, () -> xbox.getRightTriggerAxis() > 0, () -> xbox.getLeftTriggerAxis() > 0));


    xbox.getButton(Button.kRightBumper).whenPressed(new InstantCommand(() -> {
      if(intakeArmSolenoid.getPositon() != Value.kForward) {
        intakeArmSolenoid.push();
      }
    }));

    xbox.getButton(Button.kLeftBumper).whenPressed(new InstantCommand(() -> {
      if(intakeArmSolenoid.getPositon() != Value.kReverse) {
        intakeArmSolenoid.retract();
      }
    }));
  }

  public void teleopInit() {
    turret.setDefaultCommand(new TeleopTurretControl(turret, limelight, joystick::getZ, () -> joystick.getButton(1).get()));
  }

  public void disabledInit() {
    turret.clearCorrection();
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return null;
  }
}
